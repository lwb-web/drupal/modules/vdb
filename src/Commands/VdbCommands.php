<?php

namespace Drupal\vdb\Commands;

use Drush\Commands\DrushCommands;

/**
 * A drush command file.
 *
 * @package Drupal\cdi\Commands
 */
class VdbCommands extends DrushCommands {

  /**
   * Drush command that displays the given text.
   *
   * @command vdb:send
   * @aliases vdb-send
   * @usage vdb:send
   */
  public function send() {
    vdb_send();
  }

}
