<?php

namespace Drupal\vdb\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;

/**
 * Class CopyContractsForm.
 */
class CopyContractsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'copy_contracts_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Node $node = NULL) {
    $form['popup'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'popup',
        ],
      ],
    ];
    $form['popup']['headline'] = [
      '#markup' => 'Vertrag kopieren: ' . $node->getTitle(),
      '#type' => 'item',
    ];
    $form['popup']['count'] = [
      '#type' => 'textfield',
      '#title' => t('Anzahl'),
      '#default_value' => 10,
      '#required' => TRUE,
      '#prefix' => '<div class="popup-body">',
    ];
    $form['popup']['nid'] = [
      '#type' => 'hidden',
      '#value' => $node->id(),
    ];
    $form['popup']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Kopieren',
    ];
    $form['popup']['cancel'] = [
      '#type' => 'submit',
      '#value' => 'Abbrechen',
      '#suffix' => '</div>',
    ];
    $form['popup']['footer'] = [
      '#markup' => '&nbsp;',
      '#type' => 'item',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state, Node $node = NULL) {
    if (!is_numeric($form_state->getValue('count'))) {
      $form_state->setErrorByName('count', $this->t('Die Anzahl muss numerisch sein'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, Node $node = NULL) {
    // Display result.
    if ($form_state->getValue('op') !== 'Abbrechen') {
      $batch_builder = (new BatchBuilder())
        ->setTitle(t('Kopiere'))
        ->setFinishCallback('vdb_copy_batch_finished')
        ->setInitMessage(t('Import is starting'))
        ->setProgressMessage(t('Processed @current out of @total.'))
        ->setErrorMessage(t('Batch has encountered an error'));
      vdb_copy_batch($batch_builder, $form_state->getValue('nid'), $form_state->getValue('count'));
      batch_set($batch_builder->toArray());
    }
    $url = Url::fromRoute('entity.node.canonical', [
      'node' => $form_state->getValue('nid'),
    ]);
    $form_state->setRedirectUrl($url);
  }

  /**
   * Check Permission to show the Copy-Tab.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Current Node.
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultNeutral
   *   Return Access result.
   */
  public function copyContractsVisible(Node $node) {
    return AccessResult::allowedIf(
      $node->type->entity->id() === 'vdb_contract' ||
      $node->type->entity->id() === 'gesellschaftsvertrag'
    );
  }

}
