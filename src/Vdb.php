<?php

namespace Drupal\vdb;

use Drupal\cdi\Cdi\CdiTools;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\user\Entity\User;

/**
 * Class for Vdb.
 */
class Vdb {

  /**
   * Returns all recipients as array of emails.
   *
   * @param \Drupal\paragraphs\Entity\Paragraph $limit
   *   Node to get recipients.
   *
   * @return array
   *   Array of all recipients.
   */
  public static function getMailRecipients(Paragraph $limit) {
    $mails = [];
    $uids = CdiTools::loadUsersByPermission('vdb get all mails');

    $uids = array_merge($uids, Vdb::getRecipientUids($limit, 'field_benutzer', 'target_id'));
    $uids = array_merge($uids, Vdb::getRecipientUids($limit, 'field_organisationseinheiten', 'target_id', 'field_organisationalunit'));
    $uids = array_merge($uids, Vdb::getRecipientUids($limit, 'field_kostenstellen', 'target_id', 'field_costcenter'));
    $uids = array_merge($uids, Vdb::getRecipientUids($limit, 'field_funktionen', 'target_id', 'field_function'));

    foreach ($uids as $uid) {
      if ($uid == 1) {
        continue;
      }
      $account = User::load($uid);
      $mails[$uid] = $account->getEmail();
    }

    // return array('thomas.heyl@lwb.de');
    return array_unique($mails);
  }

  /**
   * Returns human readable unit.
   *
   * @param int $unit
   *   Unit as int.
   *
   * @return string
   *   Unit as string.
   */
  public static function getUnit($unit) {
    switch ($unit) {
      case 1:
        return 'day';

      case 7:
        return 'week';

      case 30:
        return 'month';

      case 365:
        return 'year';
    }
  }

  /**
   * Returns all nids to valid contracts.
   *
   * @return array
   *   Array of nids.
   */
  public static function getValidContracts() {
    $contracts = [];
    $date = new DrupalDateTime();
    $date->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $date = $date->format(DateTimeItemInterface::DATE_STORAGE_FORMAT);

    $query = \Drupal::entityQuery('paragraph')
      ->accessCheck(FALSE)
      ->condition('field_vdb_limit_next_date.value', $date, '<');
    $entity_ids = $query->execute();

    foreach($entity_ids as $entity_id) {
      $entity = \Drupal::entityTypeManager()->getStorage('paragraph')->load($entity_id);
      $data = $entity->getParentEntity();
      $runtime = $data->get('field_vdb_contract_runtime')->getValue();

      if (!empty($runtime)) {
        $start_Date = $runtime[0]['value'];
        $end_Date = $runtime[0]['end_value'];

        if ($start_Date != "1970-01-01" && $start_Date <= $date && (
            ($start_Date == $end_Date)
            ||
            ($start_Date != $end_Date && $end_Date >= $date)
          )) {
          $limits = $data->get('field_vdb_limit')->referencedEntities();

          foreach ($limits as $limit) {
            $limit_next_dates = $limit->get('field_vdb_limit_next_date')->getValue();

            if (!empty($limit_next_dates)) {
              foreach ($limit_next_dates as $limit_next_date) {
                if (strtotime($limit_next_date['value']) <= strtotime($date)) {
                  $contracts[$data->id()]['contract'] = $data;
                  $contracts[$data->id()]['limits'][] = $limit;
                }
              }
            }
          }
        }
        else {
          $data->save();
        }
      }
    }

    return $contracts;
  }

  /**
   * Returns all interval dates.
   *
   * @param int $date
   *   Date.
   * @param int $enddate
   *   Enddate.
   * @param int $interval
   *   Interval.
   * @param int $interval_unit
   *   Interval unit.
   * @param int $leadtime
   *   Leadtime.
   * @param int $leadtime_unit
   *   Leadtime unit.
   *
   * @return array
   *   Array of interval dates.
   *
   * @throws \Exception
   */
  public static function getIntervalDates($date, $enddate, $interval, $interval_unit, $leadtime, $leadtime_unit) {
    $testdate = $enddate;
    $searchdate = $enddate;
    $dates = [];

    if ($interval > 0) {
      $count = 0;

      while (TRUE) {
        $testdate = strtotime('-' . $count . ' ' . Vdb::getUnit($interval_unit), $enddate);
        $searchdate = strtotime('-' . $leadtime . ' ' . Vdb::getUnit($leadtime_unit), $testdate);
        $count = $count + $interval;

        if ($searchdate >= $date) {
          $dates[] = $searchdate;
        }
        else {
          return $dates;
        }

        if ($count > 500) {
          throw new \Exception('Der Ausführungsplan konnte nicht berechnet werden! Bitte überprüfen Sie ihre Eingabe.');
        }
      }
    }
    else {
      $searchdate = strtotime('-' . $leadtime . ' ' . Vdb::getUnit($leadtime_unit), $enddate);

      if ($searchdate >= $date) {
        $dates[] = $searchdate;
      }
    }

    return $dates;
  }

  /**
   * Returns all nids, a recipient is added to.
   *
   * @param object $account
   *   Account-Object of reader.
   * @param string $bundle
   *   Bundle.
   *
   * @return array
   *   Array of all entity ids.
   */
  public static function getNidsByUserAccess($account, $bundle) {
    $entity_ids = [];
    $u = User::load($account->id());
    
    $entity_ids = array_merge($entity_ids, Vdb::getNidsByFields('field_benutzer', $account->id(), $bundle));

    if (CdiTools::entityHasField($u, 'field_organisationalunit')) {
      $entity_ids = array_merge($entity_ids, Vdb::getNidsByFields('field_organisationseinheiten', $u->get('field_organisationalunit')->getString(), $bundle));
    }

    if (CdiTools::entityHasField($u, 'field_costcenter')) {
      $entity_ids = array_merge($entity_ids, Vdb::getNidsByFields('field_kostenstellen', $u->get('field_costcenter')->getString(), $bundle));    
    }

    if (CdiTools::entityHasField($u, 'field_function')) {
      $entity_ids = array_merge($entity_ids, Vdb::getNidsByFields('field_funktionen', $u->get('field_function')->getString(), $bundle));    
    }

    return array_unique($entity_ids, SORT_REGULAR);
  }

  /**
   * Returns all nids filtered by fields of node and it's paragraphs. 
   *
   * @param string $field_name
   *   Field to search for.
   * @param string $field_value
   *   Value to search for.   
   * @param string $bundle
   *   Bundle.
   *
   * @return array
   *   Array of all entity ids.   
   */
  private static function getNidsByFields($field_name, $field_value, $bundle) {
    $entity_ids = [];
    $limits = [];
    $filter = [];
    $filter[$field_name] = $field_value;
    $entity_ids = array_keys(CdiTools::loadEntitiesByField('node', $filter, $bundle));
    $limits = CdiTools::loadEntitiesByField('paragraph', $filter, 'vdb_limit');

    foreach($limits as $limit) {
      if (!is_null($limit->getParentEntity())) {
        $entity_ids = array_merge($entity_ids, [$limit->getParentEntity()->id()]);
      }
    }      

    return $entity_ids;
  }

  /**
   * Returns the uids of all recipiants with direct od indirect access.
   *
   * @param \Drupal\paragraphs\Entity\Paragraph $limit
   *   Node to get the values from.
   * @param string $field_name
   *   Fieldname to check user data directly.
   * @param string $column
   *   Column to check user data directly.
   * @param string $user_fieldname
   *   Fieldname to check user data from user fields.
   *
   * @return array
   *   Array of uids.
   */
  public static function getRecipientUids(Paragraph $limit, string $field_name, string $column, $user_fieldname = NULL) {
    $uids = [];

    if (CdiTools::entityHasField($limit, $field_name)) {
      foreach ($limit->get($field_name)->getValue() as $value) {
        if ($user_fieldname === NULL) {
          $uids[] = $value[$column];
        }
        else {
          $users = CdiTools::loadEntitiesByField('user',
            [$user_fieldname => $value[$column], 'status' => 1]
          );
          foreach ($users as $u) {
            $uids[] = intval($u->id());
          }
        }
      }
    }
    return $uids;
  }

}
