# VDB - Erweiterungsmodul zur Vertragsdatenbank

Das Modul stellt Funktionen bereit, die zum Betrieb einer Vertragsdatenbank notwendig sind, aber nicht mit Bordmitteln abgebildet werden können.

Fristen werden mit Hilfe des [Paragraphs](https://www.drupal.org/project/paragraphs) abgebildet.

## Features

* Berechnung Ausführungsplan: Für jede Frist berechnet das Modul einen Ausführungsplan, wann jeweils für eine Frist neachrichtigt werden muss. Dabei werden Vorlaufzeiten berücksichtigt. Falls die Berechnung mehr als 500 Einträge pro Frist ergibt, schlägt der Vorgang fehlt.
* Benachrichtigung: Anhand des Ausführungsplan in den Fristen werden Benachrichtigungen versendet. Die Empfänger werden direkt oder indirekt über Kostenstelle, Organisationseinheit oder Funktion hinterlegt. Das Modul wertet die Zuordnungen aus und benachrichtigt den Empfänger. Nach der Benachrichtigung wird der Ausführungsplan neu berechnet.
* Vertrag kopieren: Um Verträge massenhaft anlegen zu können, gibt es die Funktion einen Vertrag x mal zu kopieren. Die Kopien werden ohne Anlagen erzeugt.

## Berechtigungen

* vdb administer: Alle Verträge und Fristen ansehen, bearbeiten und löschen
* vdb read all: Leserechte für alle Verträge und Fristen
* vdb get all mails: Zu allen Fristen Emails bekommen

## Berechtigungserweiterung

Im Vertrag und auch in den Fristen können Mitarbeiter hinterlegt werden. Diese Zuordnungen können direkt oder indirekt über Kostenstelle, Organisationseinheit oder Funktion erfolgen. Das Module wertet die Zuordnungen aus und räumt dem Nutzer Leserechte für den entsprechenden Vertrag ein. 

## Voraussetzungen

* [Drupal 8](https://www.drupal.org/8)
* [Paragraphs](https://www.drupal.org/project/paragraphs)

## Drush

* vdb:send: startet die Benachrichtiung für das aktuelle Datum und die Aktualisierung der Ausführungspläne
